"""Utils"""

import io
import json
import logging
import os.path
import unicodedata
from glob import glob
from typing import Dict, List


def setup_logger(name: str, level) -> logging.Logger:
    """Create a logger."""
    logger = logging.getLogger(name)
    logger.setLevel(level)

    file_handler = logging.FileHandler(f'{name}.log')
    stream_handler = logging.StreamHandler()

    formatter = logging.Formatter(f'%(asctime)s [{name}] [%(levelname)-5.5s]  %(message)s')

    # set logging level.
    file_handler.setLevel(level)
    stream_handler.setLevel(level)

    # set formatter.
    stream_handler.setFormatter(formatter)
    file_handler.setFormatter(formatter)

    # add file and stream handlers.
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    return logger


def parse_json(file_path: str) -> Dict:
    """
    Parse json in a file.

    :param file_path: file path of the file to be parsed.

    :returns: a dictionary if parsing is successful, nothing otherwise."""  """
    """

    with io.open(file_path) as file:
        return json.load(file)


def remove_extra_spaces(text: str) -> str:
    """Remove extranious spaces."""
    trimmed = ' '.join(
        text.split()
    )

    return trimmed.strip()


def get_files(path) -> List[str]:
    """Get files in a path (recursively)."""
    path = f'{path}/**/*'
    return [*filter(os.path.isfile, glob(path, recursive=True))]


def convert_to_ascii(text: str) -> str:
    """Convert non ascii text to ascii."""
    return unicodedata.normalize('NFKD', text).encode('ascii', 'ignore').decode('ascii')
