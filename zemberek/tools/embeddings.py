"""Word2Vec Embeddings using Gensim."""
import logging
import os
import pathlib
import re
import subprocess
from multiprocessing import Manager, Pool
from operator import methodcaller as method
from uuid import uuid1

from functional import seq
from gensim.models import Word2Vec
from gensim.models.word2vec import LineSentence
from tqdm import tqdm

from .. import utils  # noqa
from ..zemberek_wrapper import Zemberek  # noqa

LOGGER = utils.setup_logger('embeddings', logging.INFO)
END_OF_QUEUE = '{{{{END_OF_QUEUE}}}}'
HTML = re.compile(r'<.*?>')


def _unit_of_work(queue, path: str, min_len: int) -> None:  # pylint: disable-all
    """
    Break text into tokenized sentences.

    i.e. "This is a sentence.", "Another sentence" => [
        ['This', 'is', 'a', 'sentence'],
        ['Another', 'sentence']
    ]

    :param text:
    """
    LOGGER.info('running preprocessing sub-task')

    try:
        # zemberek is not defined here, but in _init_pool below!
        global zemberek

        LOGGER.debug('preprocessing %s..', path)

        transformed = (
            seq.open(path, errors='ignore')

            .map(method('lower'))
            .map(utils.remove_extra_spaces)

            .map(lambda line: HTML.sub('', line))

            .flat_map(zemberek.split_sentences)  # type: ignore
            .filter(zemberek.is_turkish)  # type: ignore
            .map(zemberek.break_into_morphemes)  # type: ignore

            .map(method('split'))
            .filter(lambda tokens: len(tokens) > min_len)

            .map(' '.join)
        )

        for sentence in transformed:
            queue.put(sentence)

    except Exception as e:
        LOGGER.exception(e)


def _init_pool():
    """
    Intialize pool.

    Ran only one.
    """
    global zemberek
    zemberek = Zemberek()

    LOGGER.info('process initialized')


def _write_to_shared_file(queue, filename: str):
    """
    Write to a shared file.

    Used so multiple processes can preprocess the corpus simultaneously, and write their results to the same file,
    Without proper locking simultaneous writes would corrpt the file. this function handles locking and writing.

    :param queue: queue to get data from to write to the shared file.
    :param filename: path to the shared file.
    """
    LOGGER.info('waiting on queue..')

    try:
        file = open(filename, 'a')

        while True:
            message = queue.get(True, None)

            if message == END_OF_QUEUE:
                break

            else:
                file.write(
                    f'{message}\n'
                )

        file.close()

    except Exception as e:
        LOGGER.exception(e)


def train(directory: str, output_file: str, dim: int=300, window: int=5,
          min_len: int=5, min_count: int=3, workers_count: int=7):
    """
    Train word2vec embedding using gensim.

    :param directory: path to the directory containing the training data.
    :param output_file: file name to be used for storing the preprocessed corpus and the traind word embeddings.
    :param workers_count: number of workers to be used for preprocessing and trainging.

    for the rest of the parameters please check gensim documentation.
    """
    if workers_count < 2:
        raise ValueError('workers count has to be larger than 2.')

    corpus_dir = f'corpus-{uuid1()}'
    corpus_file = f'{output_file}.tokenized.corpus.txt'

    # create dir for preprocessed corpus.
    pathlib.Path(corpus_dir).mkdir(parents=True, exist_ok=True)

    LOGGER.info('preprocessing corpus and writing to %s', corpus_file)
    # split files into chunks of 10000 lines.
    for file in utils.get_files(directory):
        chunck_path = os.path.join(corpus_dir, f'{os.path.basename(file)}.chunck.')
        subprocess.call(f'split -l 1000 "{file}" "{chunck_path}"', shell=True)

    # get splitted files.
    files = utils.get_files(corpus_dir)

    # intialize progress bar.
    progress = tqdm(total=len(files))

    results = []
    # initialize queue.
    queue = Manager().Queue(10000)
    # create pool.
    pool = Pool(workers_count, initializer=_init_pool)

    pool.apply_async(_write_to_shared_file, (queue, corpus_file))

    # send jobs to pool.
    for file in files:
        results.append(
            pool.apply_async(
                _unit_of_work,
                (queue, file, min_len, ),
                callback=lambda *args, **kwargs: progress.update()
            )
        )

    for result in results:
        result.get()

    queue.put(END_OF_QUEUE)

    pool.close()
    pool.join()

    LOGGER.info('embeddings params window: %s, size: %s', dim, window)
    LOGGER.info('training embeddings using %d processes', workers_count)

    # preprocessing is done, now we train.
    model = Word2Vec(
        LineSentence(corpus_file),
        size=dim,
        window=window,
        min_count=min_count,
        workers=workers_count
    )

    # save trained model.
    model_file = f'{output_file}_{dim}.model'
    LOGGER.info('training is done. saving to model %s', model_file)
    model.save(model_file)
