"""Zemberek wrapper."""
# glossary for all the terms used here, i.e surface, morpheme, stem, etc ..
# https://github.com/aliok/trnltk-java/blob/master/docs/tutorial/glossary.md
import logging
import os.path
import pathlib
import warnings
from operator import methodcaller as method
from subprocess import Popen
from typing import Any, List, Optional

from functional import seq
import py4j
from py4j.java_gateway import GatewayParameters, JavaGateway, launch_gateway
from unidecode import unidecode

from zemberek import utils  # noqa

LOGGER = utils.setup_logger('zemberek', logging.INFO)

# build zemberek using "mvn package"
# .jar files will be located in target directory.

# places where we look for zemberek .jar
FILE_PATH = os.path.dirname(__file__)
BASE_URL = 'https://s3.amazonaws.com/hslate/zemberek'


ZEMBEREK_JARS = [
    f'{FILE_PATH}/../zemberek-nlp/all/target/zemberek-full.jar',
    f'{FILE_PATH}/zemberek-full.jar',
]

try:
    CLASS_PATH = seq(ZEMBEREK_JARS).filter(os.path.isfile).head()

except IndexError:
    raise FileNotFoundError('could not locate zemberek .jar file,'
                            'we expect zemberek to be in any of these paths: %s' % ZEMBEREK_JARS)


class Zemberek:
    """
    A python wrapper around zemberek-nlp.

    Currently Zemberek Morphology, Generator, Tokenzier and Language identifier are supported.
    """

    def __init__(self) -> None:
        """Initialize Zemeberek."""
        # launch java gateway.
        gateway = self.__launch_java_gateway()
        self.gateway = gateway

        # we currently use morphology and tokenization of zemberek.
        self._zemberek = self.gateway.jvm.zemberek

        # morphology
        morphology = self._zemberek.morphology
        self.morphology = morphology.TurkishMorphology.createWithDefaults()
        self.generator = self.morphology.getWordGenerator()

        # tokenization
        tokenization = self._zemberek.tokenization
        self.extractor = tokenization.TurkishSentenceExtractor.DEFAULT

        lexer = tokenization.antlr.TurkishLexer

        # varargs hacks, py4j can't dispatch functions with variable number of arguments.
        object_class = gateway.jvm.int
        object_array = gateway.new_array(object_class, 3)

        object_array[0] = lexer.Punctuation
        object_array[1] = lexer.NewLine
        object_array[2] = lexer.SpaceTab

        self.tokenizer = tokenization.TurkishTokenizer.builder().ignoreTypes(object_array).build()

        # language detection
        self.language_identifier = self._zemberek.langid.LanguageIdentifier.fromInternalModelGroup('tr_group')

        # normalization
        self.normalizer = None

        try:
            # please check morphemes.json.
            self.supported_morphemes = utils.parse_json(
                os.path.join(FILE_PATH, 'assets/morphemes.json')
            )

        except FileNotFoundError:
            raise FileNotFoundError("couldn't open morphemes.json to get supported suffixes.")

        try:
            # contains Turkish (Peoples) names.
            names = utils.parse_json(
                os.path.join(FILE_PATH, 'assets/names.json')
            )
            # convert to set for fast lookup.
            self.names = set(names)

        except FileNotFoundError:
            raise FileNotFoundError("couldn't open names.json to get Turkish names (used for NER).")

    @staticmethod
    def __launch_java_gateway() -> JavaGateway:
        """Launch java gateway."""
        port = launch_gateway(classpath=CLASS_PATH, die_on_exit=True)
        params = GatewayParameters(port=port, auto_convert=True, auto_field=True, eager_load=True)

        return JavaGateway(gateway_parameters=params)

    def break_into_morphemes(self, sentence: str) -> str:
        """
        Break a text into it's constituent stems and morphemes.

        :param sentence: sentence to be analyzed
        :return: a tuple containing the given sentence broken into it's roots and suffixes,
        suffixes are prefixed with an underscore _ and .

        ex.

        input: benım adım murat öztürk
        output: 'ben _A1sg _Gen ad _A3sg _P1sg murat _A3sg ozturk _A3sg'
        """
        # java dispatches based on signature, let's make sure
        # java is able to dispatch properly.
        assert isinstance(sentence, str)
        sentence = utils.remove_extra_spaces(sentence)

        if sentence == '':
            return sentence

        # analyze sentence.
        analysis: List[Any] = self.morphology.analyzeAndDisambiguate(sentence).bestAnalysis()

        sentence_with_morphemes: List[List[str]] = []

        for word in analysis:
            # get word root and it's morhpemes.
            morphemes_data = word.getMorphemeDataList()

            # get word stem.
            stem = word.getStem()

            # morphemes i.e. _A3sg, _Inf1. for a complete list:
            # go to https://github.com/ahmetaa/zemberek-nlp/wiki/Morphemes.
            # we currently only use the ones in suffixes.json
            morphemes = (seq(morphemes_data)
                         .map(lambda m: m.morpheme.id)
                         .filter(lambda morpheme: morpheme in self.supported_morphemes)
                         .map(lambda x: f'_{x}')
                         )

            # add word stem and morphemes, so for instance
            # götürmek ->becomes-> götür _Inf1,
            # götür being the stem and _Inf1 being the morhpemes for the infintive form.
            # all morphemes and their meanings,can be found at suffixes.json.
            sentence_with_morphemes.append(
                [stem] + morphemes.to_list()
            )

        # concat all stems and surfaces to get a string.
        return ' '.join(
            seq(sentence_with_morphemes).flatten()
        )

    def morphemes_to_sentence(self, text: str) -> str:
        """Recover sentence from it's morphemes."""
        assert isinstance(text, str)

        # merge all morphemes onto their stems.
        morphemes_merged = text.replace(' _', '_').split()

        # recovered sequence.
        recovered = []

        for word in morphemes_merged:
            # split into tokens based on _
            tokens = word.split('_')

            # add it to the recovered sequence.
            recovered.append(
                self.generate(*tokens)
            )

        return ' '.join(recovered)

    def extract_names(self, text: str) -> List[str]:
        """Extract names from a piece of text."""
        assert isinstance(text, str)
        text = utils.remove_extra_spaces(text)

        warnings.warn('this functionality relies on table lookup, '
                      'and is far from perfect. make sure it works for your case.')

        if text == '':
            return ['']

        analysis: List[Any] = self.morphology.analyzeAndDisambiguate(text).bestAnalysis()
        names = []

        for word in analysis:
            morphemes = seq(word.getMorphemeDataList()).map(lambda m: m.morpheme.id)

            # check morpheme id, Names should have an id of "Noun" (idealy).
            stem = word.getStem()

            if ('Noun' in morphemes or 'Pron' in morphemes) and unidecode(stem).upper() in self.names:
                names.append(stem)

        return names

    def extract_numbers(self, text: str) -> List[str]:
        """
        Extract numbers.

        :param text: text to extract numbers from.
        :returns: list of numbers in their input format unchanged.
        """
        assert isinstance(text, str)
        text = utils.remove_extra_spaces(text)

        warnings.warn('this functionality has some known problems, make sure it works for your case.')

        if text == '':
            return ['']

        analysis: List[Any] = self.morphology.analyzeAndDisambiguate(text).bestAnalysis()
        numbers = []

        for word in analysis:
            morphemes = seq(word.getMorphemeDataList()).map(lambda m: m.morpheme.id)

            # check morpheme id, Numbers should have an id of "Num" (idealy).
            if 'Num' in morphemes:
                numbers.append(word.getStem())

        return numbers

    def generate(self, stem: str, *morphemes) -> str:
        """
        Generate a word using a stem and some morphemes.

        for a list of morphemes, please check morphemes.json inside
        zemberek directory.

        :param stem: stem to apply morphemes to.
        :param morphemes: morphemes to apply.
        :returns: surface.
        """
        # if there are no morphemes specified
        # the same input is returned.
        if not morphemes:
            return stem

        # generate words based on stem and specified morphemes.
        generated = self.generator.generate(stem, morphemes)

        try:
            return seq(generated).last().surface

        except IndexError:
            # sometimes zemeberek results is empty,
            # in which case, input is returned.
            LOGGER.warning('zembrek was unable to apply %s to %s', morphemes, stem)
            LOGGER.warning('you can submit an issue to zemberek-nlp bug tracker at:'
                           'https://github.com/ahmetaa/zemberek-nlp/issues')
            return stem

    def split_sentences(self, text: str) -> List[str]:
        """Break text into sentences."""
        assert isinstance(text, str)
        text = utils.remove_extra_spaces(text)

        if text == '':
            return [text]

        return self.extractor.fromDocument(text)

    def tokenize(self, text: str) -> List[str]:
        """Tokenize."""
        assert isinstance(text, str)
        text = utils.remove_extra_spaces(text)

        if text == '':
            return [text]

        return [
            *map(method('getText'), self.tokenizer.tokenize(text))
        ]

    def identify_language(self, text: str) -> str:
        """Identify language of a text."""
        assert isinstance(text, str)
        text = utils.remove_extra_spaces(text)

        if text == '':
            raise ValueError('text field cannot be empty!')

        return self.language_identifier.identify(text)

    def is_turkish(self, text: str) -> bool:
        """Check whether a piece of text is Turkish or not."""
        return self.identify_language(text) == 'tr'

    @staticmethod
    def download_assets():
        """
        Download assets from S3.

        currently only normalization/spelling correction assets are stored on
        S3.
        """
        required_assets = [
            f'lm/lm.2gram.slm',

            f'normalization/ascii-map',
            f'normalization/lookup-from-graph',
            f'normalization/split',
        ]
        pathlib.Path(f'{FILE_PATH}/assets/normalization').mkdir(parents=True, exist_ok=True)
        pathlib.Path(f'{FILE_PATH}/assets/lm').mkdir(parents=True, exist_ok=True)

        tasks = []

        for missing in required_assets:
            process = Popen(
                ['wget', '-O'] +
                [f'{FILE_PATH}/assets/{missing}', f'{BASE_URL}/{missing}']
            )

            tasks.append(process)

            for task in tasks:
                task.wait()

    def _initialize_normalizer(self):
        """Initialize Turkish normalization module."""
        File = self.gateway.jvm.java.io.File  # noqa # pylint:disable-all
        Paths = self.gateway.jvm.java.nio.file.Paths  # noqa # pylint:disable-all

        root_lookup = Paths.get(
            File(f'{FILE_PATH}/assets/normalization').toURI()
        )
        language_model = Paths.get(
            File(f'{FILE_PATH}/assets/lm/lm.2gram.slm').toURI()
        )

        try:
            self.normalizer = self._zemberek.normalization.TurkishSentenceNormalizer(
                self.morphology,
                root_lookup,
                language_model
            )

        except py4j.protocol.Py4JJavaError:
            raise FileNotFoundError(
                'Failed to intialize normalization module, please do zemberek.download_assets() first. '
                'If you have already did that, it might be the case that file download was incomplete '
                'You can call download_assets again to redowloaded the files.'
            )

    def normalize(self, text: str) -> Optional[str]:
        """
        Normalize noisy text.

        :param text: text to normalize.
        :returns: normalized sentences seprated by newlines.

        https://github.com/ahmetaa/zemberek-nlp/tree/master/normalization
        """
        assert isinstance(text, str)
        text = utils.remove_extra_spaces(text)

        if text == '':
            return ''

        try:
            if self.normalizer is None:
                self._initialize_normalizer()

            return seq(self.split_sentences(text)).map(self.normalizer.normalize).make_string('\n')  # type: ignore

        except FileNotFoundError as e:
            LOGGER.info(e)

        return None
