# zemberek-python

This a pythonic wrapper around zemberek-nlp at https://github.com/ahmetaa/zemberek-nlp

### Currently version 0.16.0 of zemberek-nlp is used, zemberek is written in java, as such you need to install JVM/JDK. 

### Install using Pipenv
`pipenv install -e git+https://gitlab.com/hiddenslate/zemberek.git#egg=zemberek-python`

### Install using Pip
`pip install -e git+https://gitlab.com/hiddenslate/zemberek.git#egg=zemberek-python`


### Other os dependencies
1. wget


### Zemberek assets
https://s3.console.aws.amazon.com/s3/buckets/hslate/zemberek

To download assets required for noisy text normalization

```python
from zemberek import Zemberek
Zemberek.download_assets() # should be called one time only after zemberek is installed.
```