"""Setup module."""
from setuptools import find_packages, setup

setup(
    name='zemberek-python',
    version='0.0.1',
    url='http://gitlab.com/hiddenslate/zemberek',
    author='Abdelrahman Talaat',
    author_email='abdurrahman@hiddenslate.com',
    description=('A pythonic wrapper around Zemberek-NLP.'),
    packages=find_packages(),
    include_package_data=True,
    install_requires=['gensim', 'pyfunctional', 'tqdm', 'py4j', 'unidecode'],
)
